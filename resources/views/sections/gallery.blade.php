<div id="section-gallery" class="heading-block title-center page-section">
    <h2>รูปภาพบรรยากาศ</h2>
</div>

<ul class="portfolio-filter clearfix" data-container="#portfolio">
    <li class="activeFilter"><a href="#" data-filter="*">รูปภาพทั้งหมด</a></li>
    <li><a href="#" data-filter=".pf-air">บรรยากาศ</a></li>
    <li><a href="#" data-filter=".pf-in">สถานที่ภายใน</a></li>
    <li><a href="#" data-filter=".pf-out">เริ่มก่อสร้าง</a></li>
</ul>

<div class="portfolio-shuffle" data-container="#portfolio">
    <i class="icon-random"></i>
</div>

<div class="clear"></div>

<div id="portfolio" class="portfolio grid-container clearfix">

    <article class="portfolio-item pf-air">
        <div class="portfolio-image">
            <a href="#">
                <img src="images/gallery/1.jpg" alt="บรรยากาศ มูลนิธิศิษย์หลวงพ่อทวดเขาใหญ่">
            </a>
            <div class="portfolio-overlay">
                <a href="images/gallery/1.jpg" data-lightbox="image"><i class="icon-line-plus"></i></a>
            </div>
        </div>
    </article>

    <article class="portfolio-item pf-air">
        <div class="portfolio-image">
            <a href="#">
                <img src="images/gallery/2.jpg" alt="บรรยากาศ มูลนิธิศิษย์หลวงพ่อทวดเขาใหญ่">
            </a>
            <div class="portfolio-overlay">
                <a href="images/gallery/2.jpg" data-lightbox="image"><i class="icon-line-plus"></i></a>
            </div>
        </div>
    </article>

    <article class="portfolio-item pf-air">
        <div class="portfolio-image">
            <a href="#">
                <img src="images/gallery/3.jpg" alt="บรรยากาศ มูลนิธิศิษย์หลวงพ่อทวดเขาใหญ่">
            </a>
            <div class="portfolio-overlay">
                <a href="images/gallery/3.jpg" data-lightbox="image"><i class="icon-line-plus"></i></a>
            </div>
        </div>
    </article>

    <article class="portfolio-item pf-air">
        <div class="portfolio-image">
            <a href="#">
                <img src="images/gallery/4.jpg" alt="บรรยากาศ มูลนิธิศิษย์หลวงพ่อทวดเขาใหญ่">
            </a>
            <div class="portfolio-overlay">
                <a href="images/gallery/4.jpg" data-lightbox="image"><i class="icon-line-plus"></i></a>
            </div>
        </div>
    </article>

    <article class="portfolio-item pf-air">
        <div class="portfolio-image">
            <a href="#">
                <img src="images/gallery/5.jpg" alt="บรรยากาศ มูลนิธิศิษย์หลวงพ่อทวดเขาใหญ่">
            </a>
            <div class="portfolio-overlay">
                <a href="images/gallery/5.jpg" data-lightbox="image"><i class="icon-line-plus"></i></a>
            </div>
        </div>
    </article>

    <article class="portfolio-item pf-air">
        <div class="portfolio-image">
            <a href="#">
                <img src="images/gallery/6.jpg" alt="บรรยากาศ มูลนิธิศิษย์หลวงพ่อทวดเขาใหญ่">
            </a>
            <div class="portfolio-overlay">
                <a href="images/gallery/6.jpg" data-lightbox="image"><i class="icon-line-plus"></i></a>
            </div>
        </div>
    </article>

    <article class="portfolio-item pf-air">
        <div class="portfolio-image">
            <a href="#">
                <img src="images/gallery/7.jpg" alt="บรรยากาศ มูลนิธิศิษย์หลวงพ่อทวดเขาใหญ่">
            </a>
            <div class="portfolio-overlay">
                <a href="images/gallery/7.jpg" data-lightbox="image"><i class="icon-line-plus"></i></a>
            </div>
        </div>
    </article>

    <article class="portfolio-item pf-air">
        <div class="portfolio-image">
            <a href="#">
                <img src="images/gallery/8.jpg" alt="บรรยากาศ มูลนิธิศิษย์หลวงพ่อทวดเขาใหญ่">
            </a>
            <div class="portfolio-overlay">
                <a href="images/gallery/8.jpg" data-lightbox="image"><i class="icon-line-plus"></i></a>
            </div>
        </div>
    </article>

    <article class="portfolio-item pf-air">
        <div class="portfolio-image">
            <a href="#">
                <img src="images/gallery/9.jpg" alt="บรรยากาศ มูลนิธิศิษย์หลวงพ่อทวดเขาใหญ่">
            </a>
            <div class="portfolio-overlay">
                <a href="images/gallery/9.jpg" data-lightbox="image"><i class="icon-line-plus"></i></a>
            </div>
        </div>
    </article>

    <article class="portfolio-item pf-air">
        <div class="portfolio-image">
            <a href="#">
                <img src="images/gallery/10.jpg" alt="บรรยากาศ มูลนิธิศิษย์หลวงพ่อทวดเขาใหญ่">
            </a>
            <div class="portfolio-overlay">
                <a href="images/gallery/10.jpg" data-lightbox="image"><i class="icon-line-plus"></i></a>
            </div>
        </div>
    </article>

    <article class="portfolio-item pf-air">
        <div class="portfolio-image">
            <a href="#">
                <img src="images/gallery/11.jpg" alt="บรรยากาศ มูลนิธิศิษย์หลวงพ่อทวดเขาใหญ่">
            </a>
            <div class="portfolio-overlay">
                <a href="images/gallery/11.jpg" data-lightbox="image"><i class="icon-line-plus"></i></a>
            </div>
        </div>
    </article>

    <article class="portfolio-item pf-air">
        <div class="portfolio-image">
            <a href="#">
                <img src="images/gallery/12.jpg" alt="บรรยากาศ มูลนิธิศิษย์หลวงพ่อทวดเขาใหญ่">
            </a>
            <div class="portfolio-overlay">
                <a href="images/gallery/12.jpg" data-lightbox="image"><i class="icon-line-plus"></i></a>
            </div>
        </div>
    </article>

    <article class="portfolio-item pf-air">
        <div class="portfolio-image">
            <a href="#">
                <img src="images/gallery/13.jpg" alt="บรรยากาศ มูลนิธิศิษย์หลวงพ่อทวดเขาใหญ่">
            </a>
            <div class="portfolio-overlay">
                <a href="images/gallery/13.jpg" data-lightbox="image"><i class="icon-line-plus"></i></a>
            </div>
        </div>
    </article>

    <article class="portfolio-item pf-air">
        <div class="portfolio-image">
            <a href="#">
                <img src="images/gallery/14.jpg" alt="บรรยากาศ มูลนิธิศิษย์หลวงพ่อทวดเขาใหญ่">
            </a>
            <div class="portfolio-overlay">
                <a href="images/gallery/14.jpg" data-lightbox="image"><i class="icon-line-plus"></i></a>
            </div>
        </div>
    </article>

    <article class="portfolio-item pf-air">
        <div class="portfolio-image">
            <a href="#">
                <img src="images/gallery/15.jpg" alt="บรรยากาศ มูลนิธิศิษย์หลวงพ่อทวดเขาใหญ่">
            </a>
            <div class="portfolio-overlay">
                <a href="images/gallery/15.jpg" data-lightbox="image"><i class="icon-line-plus"></i></a>
            </div>
        </div>
    </article>

    <article class="portfolio-item pf-air">
        <div class="portfolio-image">
            <a href="#">
                <img src="images/gallery/16.jpg" alt="บรรยากาศ มูลนิธิศิษย์หลวงพ่อทวดเขาใหญ่">
            </a>
            <div class="portfolio-overlay">
                <a href="images/gallery/16.jpg" data-lightbox="image"><i class="icon-line-plus"></i></a>
            </div>
        </div>
    </article>

    <article class="portfolio-item pf-air">
        <div class="portfolio-image">
            <a href="#">
                <img src="images/gallery/17.jpg" alt="บรรยากาศ มูลนิธิศิษย์หลวงพ่อทวดเขาใหญ่">
            </a>
            <div class="portfolio-overlay">
                <a href="images/gallery/17.jpg" data-lightbox="image"><i class="icon-line-plus"></i></a>
            </div>
        </div>
    </article>

    <article class="portfolio-item pf-air">
        <div class="portfolio-image">
            <a href="#">
                <img src="images/gallery/18.jpg" alt="บรรยากาศ มูลนิธิศิษย์หลวงพ่อทวดเขาใหญ่">
            </a>
            <div class="portfolio-overlay">
                <a href="images/gallery/18.jpg" data-lightbox="image"><i class="icon-line-plus"></i></a>
            </div>
        </div>
    </article>

    <article class="portfolio-item pf-air">
        <div class="portfolio-image">
            <a href="#">
                <img src="images/gallery/19.jpg" alt="บรรยากาศ มูลนิธิศิษย์หลวงพ่อทวดเขาใหญ่">
            </a>
            <div class="portfolio-overlay">
                <a href="images/gallery/19.jpg" data-lightbox="image"><i class="icon-line-plus"></i></a>
            </div>
        </div>
    </article>

    <article class="portfolio-item pf-air">
        <div class="portfolio-image">
            <a href="#">
                <img src="images/gallery/20.jpg" alt="บรรยากาศ มูลนิธิศิษย์หลวงพ่อทวดเขาใหญ่">
            </a>
            <div class="portfolio-overlay">
                <a href="images/gallery/20.jpg" data-lightbox="image"><i class="icon-line-plus"></i></a>
            </div>
        </div>
    </article>

    <article class="portfolio-item pf-air">
        <div class="portfolio-image">
            <a href="#">
                <img src="images/gallery/21.jpg" alt="บรรยากาศ มูลนิธิศิษย์หลวงพ่อทวดเขาใหญ่">
            </a>
            <div class="portfolio-overlay">
                <a href="images/gallery/21.jpg" data-lightbox="image"><i class="icon-line-plus"></i></a>
            </div>
        </div>
    </article>

    <article class="portfolio-item pf-air">
        <div class="portfolio-image">
            <a href="#">
                <img src="images/gallery/22.jpg" alt="บรรยากาศ มูลนิธิศิษย์หลวงพ่อทวดเขาใหญ่">
            </a>
            <div class="portfolio-overlay">
                <a href="images/gallery/22.jpg" data-lightbox="image"><i class="icon-line-plus"></i></a>
            </div>
        </div>
    </article>

    <article class="portfolio-item pf-in">
        <div class="portfolio-image">
            <a href="#">
                <img src="images/gallery/23.jpg" alt="สถานที่ภายใน มูลนิธิศิษย์หลวงพ่อทวดเขาใหญ่">
            </a>
            <div class="portfolio-overlay">
                <a href="images/gallery/23.jpg" data-lightbox="image"><i class="icon-line-plus"></i></a>
            </div>
        </div>
    </article>

    <article class="portfolio-item pf-in">
        <div class="portfolio-image">
            <a href="#">
                <img src="images/gallery/24.jpg" alt="สถานที่ภายใน มูลนิธิศิษย์หลวงพ่อทวดเขาใหญ่">
            </a>
            <div class="portfolio-overlay">
                <a href="images/gallery/24.jpg" data-lightbox="image"><i class="icon-line-plus"></i></a>
            </div>
        </div>
    </article>

    <article class="portfolio-item pf-out">
        <div class="portfolio-image">
            <a href="#">
                <img src="images/gallery/25.jpg" alt="เริ่มก่อสร้าง มูลนิธิศิษย์หลวงพ่อทวดเขาใหญ่">
            </a>
            <div class="portfolio-overlay">
                <a href="images/gallery/25.jpg" data-lightbox="image"><i class="icon-line-plus"></i></a>
            </div>
        </div>
    </article>

    <article class="portfolio-item pf-out">
        <div class="portfolio-image">
            <a href="#">
                <img src="images/gallery/26.jpg" alt="เริ่มก่อสร้าง มูลนิธิศิษย์หลวงพ่อทวดเขาใหญ่">
            </a>
            <div class="portfolio-overlay">
                <a href="images/gallery/26.jpg" data-lightbox="image"><i class="icon-line-plus"></i></a>
            </div>
        </div>
    </article>

    <article class="portfolio-item pf-out">
        <div class="portfolio-image">
            <a href="#">
                <img src="images/gallery/27.jpg" alt="เริ่มก่อสร้าง มูลนิธิศิษย์หลวงพ่อทวดเขาใหญ่">
            </a>
            <div class="portfolio-overlay">
                <a href="images/gallery/27.jpg" data-lightbox="image"><i class="icon-line-plus"></i></a>
            </div>
        </div>
    </article>

    <article class="portfolio-item pf-out">
        <div class="portfolio-image">
            <a href="#">
                <img src="images/gallery/28.jpg" alt="เริ่มก่อสร้าง มูลนิธิศิษย์หลวงพ่อทวดเขาใหญ่">
            </a>
            <div class="portfolio-overlay">
                <a href="images/gallery/28.jpg" data-lightbox="image"><i class="icon-line-plus"></i></a>
            </div>
        </div>
    </article>

    <article class="portfolio-item pf-out">
        <div class="portfolio-image">
            <a href="#">
                <img src="images/gallery/29.jpg" alt="เริ่มก่อสร้าง มูลนิธิศิษย์หลวงพ่อทวดเขาใหญ่">
            </a>
            <div class="portfolio-overlay">
                <a href="images/gallery/29.jpg" data-lightbox="image"><i class="icon-line-plus"></i></a>
            </div>
        </div>
    </article>

    <article class="portfolio-item pf-out">
        <div class="portfolio-image">
            <a href="#">
                <img src="images/gallery/30.jpg" alt="เริ่มก่อสร้าง มูลนิธิศิษย์หลวงพ่อทวดเขาใหญ่">
            </a>
            <div class="portfolio-overlay">
                <a href="images/gallery/30.jpg" data-lightbox="image"><i class="icon-line-plus"></i></a>
            </div>
        </div>
    </article>

    <article class="portfolio-item pf-out">
        <div class="portfolio-image">
            <a href="#">
                <img src="images/gallery/31.jpg" alt="เริ่มก่อสร้าง มูลนิธิศิษย์หลวงพ่อทวดเขาใหญ่">
            </a>
            <div class="portfolio-overlay">
                <a href="images/gallery/31.jpg" data-lightbox="image"><i class="icon-line-plus"></i></a>
            </div>
        </div>
    </article>

    <article class="portfolio-item pf-out">
        <div class="portfolio-image">
            <a href="#">
                <img src="images/gallery/32.jpg" alt="เริ่มก่อสร้าง มูลนิธิศิษย์หลวงพ่อทวดเขาใหญ่">
            </a>
            <div class="portfolio-overlay">
                <a href="images/gallery/32.jpg" data-lightbox="image"><i class="icon-line-plus"></i></a>
            </div>
        </div>
    </article>

    <article class="portfolio-item pf-out">
        <div class="portfolio-image">
            <a href="#">
                <img src="images/gallery/33.jpg" alt="เริ่มก่อสร้าง มูลนิธิศิษย์หลวงพ่อทวดเขาใหญ่">
            </a>
            <div class="portfolio-overlay">
                <a href="images/gallery/33.jpg" data-lightbox="image"><i class="icon-line-plus"></i></a>
            </div>
        </div>
    </article>

    <article class="portfolio-item pf-out">
        <div class="portfolio-image">
            <a href="#">
                <img src="images/gallery/34.jpg" alt="เริ่มก่อสร้าง มูลนิธิศิษย์หลวงพ่อทวดเขาใหญ่">
            </a>
            <div class="portfolio-overlay">
                <a href="images/gallery/34.jpg" data-lightbox="image"><i class="icon-line-plus"></i></a>
            </div>
        </div>
    </article>

</div>

<div class="divider divider-short divider-center"><i class="icon-circle"></i></div>
