@php
if (Voyager::translatable($items)) {
$items = $items->load('translations');
}
@endphp
<div class="collapse navbar-collapse" id="navbarTogglerDemo01">
    <ul class="navbar-nav mt-2 mt-lg-0 {{ setting('site.menu-bar-position') }}">
        @foreach($items as $menu)
        @php
        $isActive = '';
        if(url($menu->link()) == url()->current()){
        $isActive = 'active';
        }
        if(url($menu->link()) == url('/').'/'.request()->segment(1)){
        $isActive = 'active';
        }
        @endphp
        @if($menu->children->isEmpty())
        <li class="nav-item {{$isActive}}">
            <a class="nav-link" href="{{ $menu->link() }}">{{ $menu->title }}
                <span class="sr-only">(current)</span>
            </a>
        </li>
        @endif
        @if(!$menu->children->isEmpty())
        <li class="nav-item dropdown">
            <a class="nav-link {{ count($menu->children) > 0 ? 'dropdown-toggle' :'' }}" href="{{ $menu->link() }}"
                id="dropdown{{ $menu->id }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{ $menu->title }}
            </a>
            @if(!$menu->children->isEmpty())
            <ul class="dropdown-menu" aria-labelledby="dropdown{{ $menu->id }}">
                @if(count($menu->children) > 0)
                @include('menu.menusub',['childs' => $menu->children,'id_sub' =>$menu->id])
                @endif
            </ul>
            @endif
        </li>
        @endif
        @endforeach
    </ul>
</div>
@push('custom-scripts')
<script>
    $('.dropdown-menu a.dropdown-toggle').on('click', function(e) {
        if (!$(this).next().hasClass('show')) {
            $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
        }
        var $subMenu = $(this).next(".dropdown-menu");
        $subMenu.toggleClass('show');


        $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
            $('.dropdown-submenu .show').removeClass("show");
        });


        return false;
        });
</script>
@endpush
