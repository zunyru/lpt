<?php

namespace App\Libs\Payments;

class PaymentEnum
{
    const GW_2C2P   = '2c2p';
    const GW_KBANK = 'kbank';
    const GW_KRUNGSRI = 'krungsri';
    const GW_CHILLPAY = 'chillpay';

    const GW_CHILLPAY_FOREGROUND    = 'chillpay-fg';
    const GW_CHILLPAY_BACKGROUND    = 'chillpay-bg';
    const GW_CHILLPAY_INQUIRY       = 'chillpay-inquiry';

    const GW_2C2P_FOREGROUND    = '2c2p-fg';
    const GW_2C2P_BACKGROUND    = '2c2p-bg';
    const GW_2C2P_INQUIRY       = '2c2p-inquiry';

    const GW_KRUNGSRI_FOREGROUND = 'krungsri-fg';
    const GW_KRUNGSRI_BACKGROUND = 'krungsri-bg';
    const GW_KRUNGSRI_INQUIRY    = 'krungsri-inquiry';

    const GW_KBANK_FOREGROUND = 'kbank-fg';
    const GW_KBANK_BACKGROUND = 'kbank-bg';
    const GW_KBANK_CHARGE    = 'kbank-create-charge';
    const GW_KBANK_TOKEN    = 'kbank-generate-token';
    const GW_KBANK_INQUIRY    = 'kbank-inquiry';
    const GW_KBANK_QR    = 'kbank-create-qr';

    const STATUS_SUCCESS    = 'success';
    const STATUS_PENDING    = 'pending';
    const STATUS_CANCEL     = 'cancel';
    const STATUS_ERROR      = 'error';
}
