<!DOCTYPE html>
<html lang="{{ config('app.locale') }}" dir="{{ __('voyager::generic.is_rtl') == 'true' ? 'rtl' : 'ltr' }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="robots" content="none" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="admin login">
    <title>Admin - {{ Voyager::setting("admin.title") }}</title>
    <link rel="stylesheet" href="{{ voyager_asset('css/app.css') }}">
    @if (__('voyager::generic.is_rtl') == 'true')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-rtl/3.4.0/css/bootstrap-rtl.css">
    <link rel="stylesheet" href="{{ voyager_asset('css/rtl.css') }}">
    @endif
    <style>
        body {
            background-image:url('{{ Voyager::image( Voyager::setting("admin.bg_image"), voyager_asset("images/bg.jpg") ) }}');

            background-color: {{Voyager::setting("admin.bg_color", "#FFFFFF")}};
        }

        body.login .login-sidebar {
            border-top:5px solid {{config('voyager.primary_color', '#22A7F0')}};
        }

        @media (max-width: 767px) {
            body.login .login-sidebar {
                border-top: 0px !important;

                border-left:5px solid {{config('voyager.primary_color', '#22A7F0')}};
            }
        }

        body.login .form-group-default.focused {
            border-color: {{config('voyager.primary_color', '#22A7F0')}};
        }

        .login-button,
        .bar:before,
        .bar:after {
            background: {{config('voyager.primary_color', '#22A7F0')}};
        }
        .remember-me-text {
            padding: 0 5px;
        }
        /* css */
        body.login .form-group .container-chbox {
            display: block;
            position: relative;
            padding-left: 35px;
            margin-bottom: 12px;
            cursor: pointer;
            font-size: 22px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
        body.login .form-group .container-chbox input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
            height: 30px;
            width: 100%;
            left: 0;
            z-index: 2;
        }
        body.login .form-group .container-chbox .checkmark {
            position: absolute;
            top: 8px;
            left: 0;
            height: 20px;
            width: 20px;
            background-color: transparent;
            border: 1px solid {{config('voyager.primary_color', '#22A7F0')}};
            border-radius: 3px;
        }
        input[type=checkbox] {
            background: #FFFFFF;
            -webkit-appearance: none;
            width: 20px;
            height: 20px;
            border: 1px solid #9CA2A8;
            border-radius: 3px;
            outline: none;
        }
        body.login .form-group .container-chbox input:checked ~ .checkmark {
            background-color: {{config('voyager.primary_color', '#22A7F0')}};
            border-color: {{config('voyager.primary_color', '#22A7F0')}};
        }
        input[type=checkbox]:checked {
            background: {{config('voyager.primary_color', '#22A7F0')}};
            border-color: {{config('voyager.primary_color', '#22A7F0')}};
        }
        input[type=checkbox]:focus{
            outline: unset;
        }
        input[type=checkbox]:checked:after {
            content: "";
            display: block;
            width: 5px;
            height: 10px;
            border: solid #FFFFFF;
            border-width: 0 2px 2px 0;
            transform: rotate(45deg);
            margin: 3px 6px;
        }
    </style>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
</head>

<body class="login">
    <div class="container-fluid">
        <div class="row">
            <div class="faded-bg animated"></div>
            <div class="hidden-xs col-sm-7 col-md-8">
                <div class="clearfix">
                    <div class="col-sm-12 col-md-10 col-md-offset-2">
                        <div class="logo-title-container">
                            <?php $admin_logo_img = Voyager::setting('admin.icon_image', ''); ?>
                            @if($admin_logo_img == '')
                            <img class="img-responsive pull-left logo hidden-xs animated fadeIn"
                                src="{{ voyager_asset('images/logo-icon-light.png') }}" alt="Logo Icon">
                            @else
                            <img class="img-responsive pull-left logo hidden-xs animated fadeIn"
                                src="{{ Voyager::image($admin_logo_img) }}" alt="Logo Icon">
                            @endif
                            <div class="copy animated fadeIn">
                                <h1>{{ Voyager::setting('admin.title', 'Voyager') }}</h1>
                                <p>{{ Voyager::setting('admin.description', __('voyager::login.welcome')) }}</p>
                            </div>
                        </div> <!-- .logo-title-container -->
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-5 col-md-4 login-sidebar">
                <div class="login-container">
                    <p>{{ __('voyager::login.signin_below') }}</p>
                    <form action="{{ route('voyager.login') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group form-group-default" id="emailGroup">
                            <label>{{ __('voyager::generic.email') }}</label>
                            <div class="controls">
                                <input type="text" name="email" id="email" value="{{ old('email') }}"
                                    placeholder="{{ __('voyager::generic.email') }}" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group form-group-default" id="passwordGroup">
                            <label>{{ __('voyager::generic.password') }}</label>
                            <div class="controls">
                                <input type="password" name="password"
                                    placeholder="{{ __('voyager::generic.password') }}" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group" id="rememberMeGroup">
                            <div class="controls">
                                <input type="checkbox" name="remember" id="remember" value="1">
                                <span class="checkmark"></span>
                                <span for="remember"
                                    class="remember-me-text">{{ __('voyager::generic.remember_me') }}</label>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-block login-button">
                            <span class="signingin hidden"><span class="voyager-refresh"></span>
                                {{ __('voyager::login.loggingin') }}...</span>
                            <span class="signin">{{ __('voyager::generic.login') }}</span>
                        </button>
                    </form>
                    <div style="clear:both"></div>
                    @if(!$errors->isEmpty())
                    <div class="alert alert-red">
                        <ul class="list-unstyled">
                            @foreach($errors->all() as $err)
                            <li>{{ $err }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                </div> <!-- .login-container -->
            </div> <!-- .login-sidebar -->
        </div> <!-- .row -->
    </div> <!-- .container-fluid -->
    <script>
        var btn = document.querySelector('button[type="submit"]');
    var form = document.forms[0];
    var email = document.querySelector('[name="email"]');
    var password = document.querySelector('[name="password"]');
    btn.addEventListener('click', function(ev){
        if (form.checkValidity()) {
            btn.querySelector('.signingin').className = 'signingin';
            btn.querySelector('.signin').className = 'signin hidden';
        } else {
            ev.preventDefault();
        }
    });
    email.focus();
    document.getElementById('emailGroup').classList.add("focused");

    // Focus events for email and password fields
    email.addEventListener('focusin', function(e){
        document.getElementById('emailGroup').classList.add("focused");
    });
    email.addEventListener('focusout', function(e){
       document.getElementById('emailGroup').classList.remove("focused");
    });

    password.addEventListener('focusin', function(e){
        document.getElementById('passwordGroup').classList.add("focused");
    });
    password.addEventListener('focusout', function(e){
       document.getElementById('passwordGroup').classList.remove("focused");
    });

    </script>
</body>

</html>
