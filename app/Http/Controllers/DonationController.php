<?php

namespace App\Http\Controllers;

use App\Repositories\TransactionRepository;
use Illuminate\Http\Request;

class DonationController extends Controller
{
    protected $transactionRepo;

    public function __construct
    (TransactionRepository $transactionRepository)
    {
        $this->transactionRepo = $transactionRepository;
    }

    function donation(Request $request)
    {
        $validatedData = $request->validate([
            'amount' => 'required|numeric',
            'phone' => 'required',
        ], [
            'amount.required' => 'กรุณาระบุจำนวนยอดเงินบริจาค',
            'amount.numeric' => 'กรุณาระบุจำนวนยอดเงินบริจาคเป็นตัวเลข',
            'phone.required' => 'กรุณาระบุเบอร์โทรศัพท์'
        ]);

        $amount = $request->amount;
        $phone = $request->phone;
        $ip = $request->ip();

        $data = $this->transactionRepo->store(array_merge($request->all(), [
                'ip' => $ip,
                'user_agent' => $request->userAgent(),
                'payment_type' => 'ChillPay'
            ])
        );

        return view('payments.chillpay.template',
            compact(
                'data'
            )
        );
    }

    //TODO::KBank
    public function kbank_view(Request $request)
    {
        $validatedData = $request->validate([
            'amount' => 'required|numeric',
        ], [
            'amount.required' => 'กรุณาระบุจำนวนยอดเงินบริจาค',
            'amount.numeric' => 'กรุณาระบุจำนวนยอดเงินบริจาคเป็นตัวเลข',
        ]);

        $amount = $request->amount;
        $phone = $request->phone;
        $ip = $request->ip();

        $data = $this->transactionRepo->store(array_merge($request->all(), [
                'ip' => $ip,
                'user_agent' => $request->userAgent(),
                'payment_type' => 'KBANK'
            ])
        );

        $amount = str_replace(',', '', number_format($request->amount, 2));

        return view('payments.kbank.inline',
            compact(
                'amount',
                'data'
            )
        );
    }

    public function checkout(Request $request)
    {
        dd($request->all());
    }
}
