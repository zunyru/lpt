<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\MessageBag;
use stdClass;

class ApiController extends Controller
{
    protected $status;
    protected $data;
    protected $responseData;
    protected $messageBag;
    protected $message;
    protected $meta;
    protected $count;
    protected $success;
    protected $isException;

    public function __construct()
    {
        $this->status = JsonResponse::HTTP_OK;
        $this->messageBag = new MessageBag();
        $this->data = [];
        $this->responseData = [];
        $this->count = 0;
        $this->success = true;
        $this->meta = NULL;
        $this->isException = false;
    }

    protected function getMeta()
    {
        $meta = new stdClass();

        if ($this->data instanceof LengthAwarePaginator) {
            $meta->total_item_current_page = $this->data->total();
            $meta->item_per_page = $this->data->perPage();
            $meta->current_page = $this->data->currentPage();
            $meta->total_item = $this->data->lastItem();
            $meta->total_page = $this->data->lastPage();
        } elseif ($this->data instanceof Collection) {
            $meta->total_item_current_page = $this->data->count();
            $meta->item_per_page = $this->data->count();
            $meta->current_page = 1;
            $meta->total_item = $this->data->count();
            $meta->total_page = 1;
        } else {
            return null;
        }

        return $meta;
    }

    protected function getData()
    {
        if ($this->data instanceof LengthAwarePaginator) {
            $data = $this->data->getCollection();
        } elseif (is_subclass_of($this->data, Model::class)) {
            $data = $this->data;
        } elseif ($this->data instanceof Collection) {
            $data = $this->data->toArray();
        } elseif (empty($this->data)) {
            $data = [];
        } else {
            $data = $this->data;
        }

        return $data;
    }

    protected function getCount()
    {
        if ($this->data instanceof LengthAwarePaginator) {
            $count = $this->data->total();
        } else {
            if (method_exists($this->data, 'count')) {
                $count = $this->data->count();
            } elseif (is_array($this->data)) {
                $count = count($this->data);
            } elseif (is_object($this->data)) {
                $count = 1;
            } else {
                $count = 0;
            }
        }

        return $count;
    }

    /**
     * @return stdClass
     */
    protected function getResponse()
    {
        $response = new stdClass();

        $response->data = $this->getData();
        $response->success = $this->success;
        $response->count = $this->getCount();
        $response->message = $this->message;
        $response->status = $this->status;

        $meta = $this->getMeta();
        if (!empty($meta)) {
            $response->meta = $meta;
        }

        return $response;
    }

    public function response($data = NULL, int $status = JsonResponse::HTTP_OK, string $message = NULL, $success = true)
    {
        $this->status = $status ?? JsonResponse::HTTP_OK;
        $this->data = $data;
        if (!empty($message)) {
            $this->message = $message;
        } else {
            $this->message = __('responses.success');
        }
        $this->success = $success;

        return response()->json($this->getResponse(), $this->status);
    }

    public function responseError(string $message = NULL, int $status = JsonResponse::HTTP_BAD_REQUEST, string $attribute = '')
    {
        $this->status = $status ?? JsonResponse::HTTP_BAD_REQUEST;
        $this->success = false;

        // localize message
        if (!empty($attribute)) {
            $this->message = __($message, ['attribute' => $attribute]);
        } else {
            $this->message = __($message, ['attribute' => '']);
        }

        return response()->json($this->getResponse(), $this->status);
    }

    /* use App/Exceptions/Handler@customApiResponse instead
    public function responseNotFound($message = '')
    {
        if (empty($message)) {
            $message = __('exceptions.not_found');
        }
        return $this->responseError(JsonResponse::HTTP_NOT_FOUND, $message);
    }*/

}
