<?php

namespace App\Traits;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

trait QueueLoggingTrait
{
    protected function className()
    {
        $paths = explode('\\', __CLASS__);
        return array_pop($paths);
    }

    protected function info($message, $context = [])
    {
        $class = $this->className();
        Log::info("{$class}:{$message}", $context);
    }

    protected function debug($message, $context = [])
    {
        $class = $this->className();
        Log::debug("{$class}:{$message}", $context);
    }
}
