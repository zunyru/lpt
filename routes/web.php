<?php

use App\Http\Controllers\DonationController;
use App\Http\Controllers\Payments\GwChillpayController;
use App\Http\Controllers\Payments\GwKBankController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/donate', function () {
    return view('payments.donate');
})->name('donate');

Route::any('/donation-channel', [DonationController::class, 'donation'])->name('donation');

Route::any('/donation-kbank/', [DonationController::class, 'kbank_view'])->name('kbank-view');

Route::post('/checkout', [GwKBankController::class, 'charge'])->name('charge');

Route::any('/payments/kbank', [GwKBankController::class, 'redirect']);

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();

    //roles update
    Route::match(['put', 'patch'], 'my-roles/{role}', [Voyager\VoyagerRoleController::class, 'update'])
        ->name('voyager.roles.my-update');

    //setting
    Route::get('/websetting', [Voyager\VoyagerSettingsController::class, 'websetting'])
        ->name('websetting.index');

    //summernote
    Route::post('summernote', [Formfield\SummernoteController::class, 'upload']);

    //slug
    Route::post('/check/check-dup-title/', [CheckduptitleController::class, 'checkDupTitle'])
        ->name('check.check-dup-title');

    Route::post('/check/check-dup-slug/', [CheckduptitleController::class, 'checkDupSlug'])->name('check.check-dup-slug');

});


//WebHook Notify API for Card Payment
Route::any('/payments/kbank-redirect', [GwKBankController::class, 'background']);