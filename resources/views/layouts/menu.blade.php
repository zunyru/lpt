<header id="header">
    <div id="header-wrap">
        <div class="container clearfix">
            <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

            <div id="logo">
                <a href="/" class="standard-logo" data-dark-logo="{{ asset('images/logo.png') }}"><img src="{{ asset('images/logo.png') }}" alt="มูลนิธิศิษย์หลวงพ่อทวดเขาใหญ่"></a>
                <a href="/" class="retina-logo" data-dark-logo="{{ asset('images/logo@2x.png') }}"><img src="{{ asset('images/logo@2x.png') }}" alt="มูลนิธิศิษย์หลวงพ่อทวดเขาใหญ่"></a>
            </div>

            <nav id="primary-menu">
                <ul class="one-page-menu">
                    <li class="current"><a href="#" data-href="#section-home"><div>หน้าแรก</div></a></li>
                    <li><a href="{{ route('donate') }}" ><div>บริจาค</div></a></li>
                    <li><a href="#" data-href="#section-foundation"><div>มูลนิธิ</div></a></li>
                    <li><a href="#" data-href="#section-gallery"><div>บรรยากาศ</div></a></li>
                    <li><a href="#" data-href="#section-contact"><div>ติดต่อเรา</div></a></li>
                </ul>
            </nav>
        </div>
    </div>
</header>
