<?php

namespace App\Libs;

use App\Libs\Payments\PaymentEnum;
use App\Libs\Payments\PaymentGwInterface;
use App\Libs\Payments\Response;
use App\Models\PaymentType;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;
use Throwable;

class PGwKBank implements PaymentGwInterface
{
    /**
     * -Initialize : Payment transaction initialized (process)
     * -Pre-Authorized : Payment need to do authentication 3D secure (process)
     * -Authorized : Authorized success-Declined : Reject payment from host (process)
     * -Reversed : Payment failed from system reject (error)
     * -Voided : Void transaction complete (cancel)
     * -Captured : Settle transaction success (wait for complete in  next day) (success)
     * -Settled : Payment transaction settled-Refund Sent : Successfully refund (wait for completion the next day) (cancel)
     * -Refunded : Payment transaction refunded (cancel)
     * -Partial Refund : Some of amount refunded (cancel)
     */

    const INQUIRY_STATUS_SETTLED = 'Captured';

    const TRANSACTION_STATE_AUTHORIZED = 'Authorized'; // Authorized success

    const TRANSACTION_STATE_INITIALIZE = 'Initialize'; // Payment transaction initialized
    const TRANSACTION_STATE_PRE_AUTHORIZED = 'Pre-Authorized'; // Payment need to do authentication 3D secure
    const TRANSACTION_STATE_DECLINED = 'Declined'; // Reject payment from host
    const TRANSACTION_STATE_REVERSED = 'Reversed'; // Payment failed from system reject
    const TRANSACTION_STATE_VOIDED = 'Voided';
    const TRANSACTION_STATE_SETTLED = 'Settled';
    const TRANSACTION_STATE_REFUNDED = 'Refunded';
    const TRANSACTION_STATE_PARTIAL_REFUND = 'Partial Refund';

    /**
     * request
     *
     * @param mixed $ref_id order ref_id
     * @param number $amount
     * @param string $description
     * @return array
     */
    public static function request($ref_id, $amount, $description = '', $params = [])
    {
        $req = new PGwKBankRequest($ref_id, $amount, $description, $params);
        return $req->values();
    }

    public static function charge($ref_id, $amount, $description = '', $params = [])
    {
        $amount = number_format($amount, 2, '.', '');

        $charge = new PGwKBankCharge(array_merge(
            $params,
            [
                'ref_id' => $ref_id,
                'amount' => $amount,
                'description' => $description,
            ]
        ));

        return $charge->get();
    }

    /**
     * decode kbank request
     *
     * @param array $params Kbank post body
     * @return \App\Libs\Payments\Response
     */
    public static function decode($params)
    {
        switch (@$params['transaction_state']) {
            case self::TRANSACTION_STATE_DECLINED:
            case self::TRANSACTION_STATE_REVERSED:
            case self::TRANSACTION_STATE_VOIDED:
            case self::TRANSACTION_STATE_REFUNDED:
            case self::TRANSACTION_STATE_PARTIAL_REFUND:
                return new Response(PaymentEnum::STATUS_CANCEL, $params, PaymentEnum::GW_KBANK);
        }

        $ver = new PGwKBankVerify($params);
        $result = $ver->get();

        if ($result) {
            $status = PaymentEnum::STATUS_SUCCESS;
            $params['charge_id'] = $params['id'];
        } else {
            $status = PaymentEnum::STATUS_ERROR;
        }

        return new Response($status, $params, PaymentEnum::GW_KBANK);
    }

    public static function redirect($params)
    {
        $status = ($params['status'] === 'true')
            ? PaymentEnum::STATUS_SUCCESS
            : PaymentEnum::STATUS_ERROR;

        if ($status) {
            $params['charge_id'] = $params['objectId'];
        }

        return new Response($status, $params, PaymentEnum::GW_KBANK);
    }

    /**
     * inquiry order status
     *
     * @param mixed $order_id order uuid
     * @return Response
     */
    public static function inquiry($order)
    {

        $inquiry = new PGwKBankInquiry(
            $order->ref_id,
            $order->payment_type_id
        );
        $result = $inquiry->get();

        if (!$result) {
            return new Response(PaymentEnum::STATUS_ERROR, null, PaymentEnum::GW_KBANK);
        }

        switch (@$result['transaction_state']) {
            case self::INQUIRY_STATUS_SETTLED:
            case self::TRANSACTION_STATE_AUTHORIZED:
                $status = PaymentEnum::STATUS_SUCCESS;
                break;
            case self::TRANSACTION_STATE_INITIALIZE:
            case self::TRANSACTION_STATE_PRE_AUTHORIZED:
                $status = PaymentEnum::STATUS_PENDING;
                break;
            default:
                $status = PaymentEnum::STATUS_ERROR;
                break;
        }

        return new Response($status, $result, PaymentEnum::GW_KBANK);
    }
}

class PGwKBankRequest
{
    // thai baht
    const CURRENCY = "THB";
    const PAYMENT_METHOD = 'card';

    protected $params = [];

    function __construct($order_id, $amount, $description = '', $params)
    {
        $ref_id = $params['ref_id'];
        $payment_type_id = $params['payment_type_id'];
        $amount = number_format($amount, 2, '.', '');

        // installment
        /*$installment_id = PaymentType::P_KBANK_INSTALLMENT[PaymentType::ID];
        $is_installment = $payment_type_id === $installment_id;
        $smartpay_id = ($is_installment)
            ? config('payment.kbank.smartpay.smartpay_id')
            : null;

        $smartpay_term = ($is_installment)
            ? config('payment.kbank.smartpay.term')
            : null;

        // mechant id
        $merchant_id = ($is_installment)
            ? config('payment.kbank.merchant_installment_id')
            : config('payment.kbank.merchant_id'); // full*/

        // generate params
        $params = [
            'ref_id' => $ref_id,
            'amount' => $amount,
            'currency' => self::CURRENCY,
            'method' => self::PAYMENT_METHOD,
            'merchant_id' => env('KBANK_MID'),
            'public_apikey' => env('KBANK_APIKEY'),
            'action_url' => route('payments-kbank-charge'),
            'script_url' => env('KBANK_JAVASCRIPT_URL'),
        ];

        $this->params = $params;
    }

    public function values()
    {
        return $this->params;
    }
}

class PGwKBankCharge
{
    const MERCHANTS_BEAR_INTEREST = '0001'; //merchants bear interest
    const CUSTOMERS_BEAR_INTEREST = '0002'; //customers bear interest

    const INQUIRY_STATUS_SETTLED = 'Captured';
    const TRANSACTION_STATE_AUTHORIZED = 'Authorized'; // Authorized success

    const TRANSACTION_STATE_INITIALIZE = 'Initialize'; // Payment transaction initialized
    const TRANSACTION_STATE_PRE_AUTHORIZED = 'Pre-Authorized'; // Payment need to do authentication 3D secure
    const TRANSACTION_STATE_DECLINED = 'Declined'; // Reject payment from host
    const TRANSACTION_STATE_REVERSED = 'Reversed'; // Payment failed from system reject
    const TRANSACTION_STATE_VOIDED = 'Voided';
    const TRANSACTION_STATE_SETTLED = 'Settled';
    const TRANSACTION_STATE_REFUNDED = 'Refunded';
    const TRANSACTION_STATE_PARTIAL_REFUND = 'Partial Refund';

    protected $params = [];

    public function __construct($params)
    {
        $this->params = $params;
    }

    protected function getClient()
    {
        $secret_key = env('SECRET_KEY');
        return Http::withHeaders([
            'Content-Type' => 'application/json',
            'x-api-key' => $secret_key,
        ])->timeout(10);
    }

    private function request($body = [])
    {
        $action_url = env('KBANK_INQUIRY_CHARGE');
        $client = $this->getClient();
        $response = null;

        try {
            $response = $client->post($action_url, $body);
        } catch (\Throwable $e) {
            return $e;
        }

        return $response;
    }

    public function get()
    {
        $params = $this->params;

        $body = [
            'amount' => $params['amount'],
            'currency' => PGwKBankRequest::CURRENCY,
            'description' => $params['description'],
            'source_type' => $params['payment_methods'], // paymentMethods
            'mode' => 'token',
            'reference_order' => $params['ref_id'],
            'token' => $params['token'],
            //'order_id' => $params['ref_id'],
            'ref_1' => $params['ref_id'],
            'ref_2' => Carbon::now()->toIso8601ZuluString(),
        ];

        // additional for installment
        if (isset($params['smartpay_id'])) {
            $body['additional_data'] = [
                'mid' => $params['merchant_id'],
                'smartpay_id' => $params['smartpay_id'],
                'term' => $params['smartpay_term'],
            ];
        }

        $response = $this->request($body);

        // got exception
        if (is_a($response, Throwable::class)) {
            return new Response(
                PaymentEnum::STATUS_ERROR,
                ['message' => $response->getMessage()],
                PaymentEnum::GW_KBANK
            );
        }

        $data = $response->json();

        if ($response->ok()) {
            switch (@$data['transaction_state']) {
                case self::INQUIRY_STATUS_SETTLED:
                case self::TRANSACTION_STATE_AUTHORIZED:
                    $status = PaymentEnum::STATUS_SUCCESS;
                    break;
                case self::TRANSACTION_STATE_INITIALIZE:
                case self::TRANSACTION_STATE_PRE_AUTHORIZED:
                    $status = PaymentEnum::STATUS_PENDING;
                    break;
                default:
                    $status = PaymentEnum::STATUS_ERROR;
                    break;
            }
        } else {
            $status = PaymentEnum::STATUS_ERROR;
        }

        return new Response($status, $data, PaymentEnum::GW_KBANK);
    }
}

class PGwKBankInquiry
{
    protected $secret_key;
    protected $order_id;
    protected $action_url;

    public function __construct($order_id, $payment_type_id)
    {
        (in_array($payment_type_id, [
            PaymentType::P_KBANK_FULL[PaymentType::ID],
            PaymentType::P_KBANK_DEPOSIT[PaymentType::ID],
            PaymentType::P_KBANK_INSTALLMENT[PaymentType::ID],
        ]))
            ? $url = config('payment.kbank.transaction.inquiry_charge') . $order_id
            : $url = config('payment.kbank.transaction.inquiry_qr') . $order_id;

        $this->order_id = $order_id;
        $this->action_url = $url;
        $this->secret_key = config('payment.kbank.secret_key');
    }

    public function get()
    {
        $response = Http::withHeaders([
            'Content-Type' => 'application/json',
            'x-api-key' => $this->secret_key,
        ])->get($this->action_url);

        return ($response->ok())
            ? $response->json()
            : [];
    }
}

/**
 * Class PGwKBankVerify  as Settle Transaction
 * @package App\Libs
 */
class PGwKBankVerify
{
    protected $params;

    public function __construct($params)
    {
        $this->params = $params;
    }

    public function get()
    {
        $secret_key = env('SECRET_KEY');
        $params = $this->params;

        $id = $params['id'] ?? '';
        $amount = (float) ($params['amount'] ?? 0.00);
        $amount = (string) (number_format($amount, 4, '.', ''));
        $currency = $params['currency'] ?? '';
        $status = $params['status'] ?? '';
        $transaction_state = $params['transaction_state'] ?? '';
        $checksum = $params['checksum'] ?? '';

        //id (Charge ID) + amount (convert to string format to 4 decimal places) + currency + status + transaction_state + salt (salt is merchant secret key)
        $value = $id
            . $amount
            . $currency
            . $status
            . $transaction_state
            . $secret_key;

        $hash = hash('sha256', $value);

        return (strcmp(strtolower($checksum), strtolower($hash)) === 0);
    }
}
