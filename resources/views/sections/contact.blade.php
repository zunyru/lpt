<div id="section-contact">
    <div class="fancy-title title-dotted-border">
        <h3>ติดต่อเรา</h3>
    </div>
    <div class="heading-block title-center page-section">
        <h2>ที่อยู่</h2>
        <span>118 อาณาจักรหลวงพ่อทวดเขาใหญ่ องค์ใหญ่ที่สุดในโลก ตำบล หมูสี อำเภอปากช่อง นครราชสีมา 30450</span>
        <!-- Google Map -->


    </div>
</div><!-- contact-widget End -->

<!-- Contact Info -->
<div class="col_full nobottommargin clearfix">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3861.3558182226798!2d101.42732961484!3d14.578789589816402!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x311c2daf4eb91e87%3A0xb3205e33178df9a5!2z4Lir4Lil4Lin4LiH4Lie4LmI4Lit4LiX4Lin4LiU4LmA4LiC4Liy4LmD4Lir4LiN4LmIIOC4reC4h-C4hOC5jOC5g-C4q-C4jeC5iOC4l-C4teC5iOC4quC4uOC4lOC5g-C4meC5guC4peC4gQ!5e0!3m2!1sth!2sth!4v1603047646107!5m2!1sth!2sth"  frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
</div>
