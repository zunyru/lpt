@extends('layouts.app')
<style>
    #footerChillPay {
        display: none;
    }

    #mmp-payment-method-wrap {
        box-shadow: 0 .5rem 1rem rgba(0, 0, 0, .15) !important;
        border: none !important;
    }

    #mmp-paymentmethod-wrap .payment-method-action button {
        background: #590000 !important;
    }

    .payment-method-logo {
        display: none;
    }
</style>
@section('content')

    <form id="payment-form" action="{{ env('POST_URL') }}}" method="post" role="form" class="form-horizontal">
        <modernpay:widget id="modernpay-widget-container"
                          data-merchantid="{{ config('chillpay.merchant_id') }}"
                          data-amount="{{ str_replace(".","",$data->amount) }}"
                          data-orderno="{{ $data->refId }}"
                          data-customerid="{{ $data->uuid }}"
                          data-mobileno="{{ $data->phone }}"
                          data-clientip="{{ $data->ip }}"
                          data-lang="TH"
                          data-routeno="1"
                          data-currency="764"
                          data-description="บริจาคเงิน"
                          data-apikey="{{ config('chillpay.api_key') }}">
        </modernpay:widget>
        {{--<button type="submit" id="btnSubmit" value="Submit" class="btn">Payment</button>--}}
    </form>
    <script async src="{{env('SCRIPT_POST')}}" charset="utf-8"></script>
@endsection
