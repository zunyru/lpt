<?php


namespace App\Libs;


use App\Libs\Payments\PaymentEnum;
use App\Libs\Payments\Response;
use Illuminate\Support\Facades\Log;

class PGwChillpayVerify
{

    protected $params;

    public function __construct($params)
    {
        $this->params = $params;
    }

    public function get()
    {
        $secret_key = config('chillpay.secret_key');
        $params = $this->params;

        $value = $params['TransactionId'] . $params['Amount'] . $params['OrderNo'] . $params['CustomerId'] . $params['BankCode'] . $params['PaymentDate'] . $params['PaymentStatus'] . $params['BankRefCode'] . $params['CurrentDate'] . $params['CurrentTime'] . $params['PaymentDescription'] . $params['CreditCardToken'] . $params['Currency'].$secret_key;

        $CheckSum = $params['CheckSum'];
        

        $hash = md5($value);
        //dd($value, $test,$secret_key, $hash, $CheckSum);
        if ($CheckSum == $hash) {
            return true;
        } else {
            return false;
        }
    }
}
