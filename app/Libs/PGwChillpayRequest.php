<?php


namespace App\Libs;


class PGwChillpayRequest
{
    const CURRENCY_NUMBER = 764;
    const CURRENCY_DIGIT = 2;

    protected $params = [];

    /**
     * PGwChillpayRequest constructor.
     * @param $order_id
     * @param $amount
     * @param mixed|string $description
     * @param array|mixed $params
     */
    public function __construct($order_id, $amount, string $description, array $params)
    {
        $this->params = [
            'MerchantCode' => '',
            'OrderNo' => $order_id,
            'CustomerId' => 'Cus'.$order_id,
            'Amount' => $amount
        ];
    }

    public function values()
    {
        return $this->params;
    }
}