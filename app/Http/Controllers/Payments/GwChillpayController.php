<?php

namespace App\Http\Controllers\Payments;

use App\Http\Controllers\ApiController;
use App\Libs\Payments\PaymentEnum;
use App\Libs\PGwChillpay;
use App\Libs\PGwChillpayVerify;
use App\Repositories\TransactionRepository;
use App\Repositories\PaymentRepository;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class GwChillpayController extends ApiController
{
    protected $transaction_repo;

    public function __construct(
        PaymentRepository $payment,
        TransactionRepository $transaction
    )
    {
        $this->transaction_repo = $transaction;

        // middleware
        $middleware_log = function ($gw) use ($payment) {
            return function ($req, $next) use ($gw, $payment) {
                $payment->log($req, $gw);
                return $next($req);
            };
        };

        $middleware_allow = function ($req, $next) {
            return $next($req);
        };

        $this->middleware($middleware_log(PaymentEnum::GW_CHILLPAY_FOREGROUND))->only(['redirect']);
        $this->middleware($middleware_log(PaymentEnum::GW_CHILLPAY_BACKGROUND))->only(['background']);
        $this->middleware($middleware_allow)->only(['background']);
    }


    public function redirect(Request $req)
    {
        // payment processing
        $payment = PGwChillpay::decode($req->input());

        $order = $this->transaction_repo->getByPaymentResp($payment);
        $log_payment = $this->transaction_repo->storeLogPayment($payment);

        if (!$order) {
            return 'order not found';
        }

        $no = $payment->input('orderNo');
        if ($payment->success()) {
            $this->transaction_repo->paymentProcess($payment);
            $status = true;
            return view('layouts.thankyou', compact('no', 'status'));
        } else if ($payment->cancel()) {
            $this->transaction_repo->paymentCancel($payment);
            $status = false;
            return view('layouts.thankyou', compact('no', 'status'));
        }

    }

    public function background(Request $req)
    {
        Log::channel('chillpay')->info($req);
        // payment processing
        $payment = PGwChillpay::decode_bg($req->input());

        if ($payment->success()) {
            $order = $this->transaction_repo->paymentApprove($payment);
            // response
            return $this->response('ok');
        }else  {
            $this->transaction_repo->paymentCancel($payment);
            return $this->response('ok');
        }

        return $this->response('flase');

    }
}
