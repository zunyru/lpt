<li class="nav-item dropdown">
    @php
    if (Voyager::translatable($items)) {
    $items = $items->load('translations');
    }
    @endphp
    @foreach ($items as $menu_item)
    @php
    $originalItem = $menu_item;
    if (Voyager::translatable($menu_item)) {
    $menu_item = $menu_item->translate($options->locale);
    }
    $isActive = null;
    $styles = null;
    $icon = null;
    // Background Color or Color
    if (isset($options->color) && $options->color == true) {
    $styles = 'color:'.$menu_item->color;
    }
    if (isset($options->background) && $options->background == true) {
    $styles = 'background-color:'.$menu_item->color;
    }
    // Check if link is current
    if(url($menu_item->link()) == url()->current()){
    $isActive = 'active';
    }
    // Set Icon
    if(isset($options->icon) && $options->icon == true){
    $icon = '<i class="' . $menu_item->icon_class . '"></i>';
    }
    @endphp
<li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Dropdown</a>
    <div class="dropdown-menu">
        <a class="dropdown-item" href="#">Link 1</a>
        <a class="dropdown-item" href="#">Link 2</a>
        <a class="dropdown-item" href="#">Link 3</a>
    </div>
</li>
@endforeach
</li>
