<?php

namespace App\Http\Controllers\Payments;

use App\Http\Controllers\ApiController;
use App\Libs\Payments\PaymentEnum;
use App\Libs\Payments\Response;
use App\Libs\PGwKBank;
use App\Libs\PGwKBankRequestQr;
use App\Repositories\OrderRepository;
use App\Repositories\PaymentRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class GwKBankController extends ApiController
{
    protected $order_repo;
    protected $payment;
    protected $smartpay;

    public function __construct(
        PaymentRepository $payment,
        OrderRepository $order
    ) {
        $this->order_repo = $order;
        $this->payment = $payment;
    }

    /**
     * request
     */
    public function request(Request $req)
    {
        $this->validate($req, [
            'order_id' => 'required|string',
            'html' => 'string',
        ]);

        $order_id = $req->input('order_id');
        $html = $req->input('html');

        $order = ($order_id)
            ? $this->order_repo->getByRefId($order_id)
            : null;

        if (!$order) {
            throw new \Exception('not found');
        }

        $tmps = [
            'ref_id' => $order->uuid,
            'payment_type_id' => null,
        ];

        $params = PGwKBank::request($order->ref_id, $order->amount, '', $tmps);

        return ($html)
            ? view('payments.kbank.embed', $params)
            : $this->response($params);
    }

    /**
     * charge
     */
    public function charge(Request $req)
    {
        $this->validate($req, [
            'order_id' => 'required|string',
            'token' => 'required|string',
        ]);

        // convert input key into snake_case
        $inputs = [];
        foreach ($req->input() as $k => $v) {
            $inputs[Str::snake($k)] = $v;
        }

        $ref_id = $inputs['order_id'];
        $order = ($ref_id)
            ? $this->order_repo->getByRefId($ref_id)
            : null;

        if (!$order) {
            return display_error('order not found');
        }

        $amount = $order->amount;
        $description = 'บริจาคเงิน';

        $params = [
            'merchant_id' => $inputs['mid'],
            'token' => $inputs['token'],
            'payment_methods' => $inputs['payment_methods'],
            'smartpay_id' => $inputs['smartpay_id'] ?? null,
            'smartpay_term' => $inputs['term'] ?? null,
        ];

        // call charge API
        $response = PGwKBank::charge(
            $ref_id,
            $amount,
            $description,
            $params,
        );

        // get error
        if ($response->error()) {
            $message = $response->input('message')
                ?? $response->input('failure_message');
            return $message;
        }

        // keep charge_id as a transaction_ref (aka chrg_id)
        $order->transaction_ref = $response->input('id');
        try {
            $order->save();
        } catch (\Exception $e) {

        }

        // need to redirect (3SD)
        $redirect_url = $response->input('redirect_url');
        if ($redirect_url) {
            return redirect($redirect_url, 301);
        }

        // no redirect ?
        // check merchant account setting (3DS or not) >> TODO
        // assume call redirect
        $reqx = new Request([
            'objectId' => $response->input('id'),
            'token' => $response->input('token'),
            'status' => ($response->input('status') === 'success') ? 'true' : 'false',
        ]);

        return $this->redirect($reqx);
    }

    public function redirect(Request $req)
    {
        // payment processing
        $payment = PGwKBank::redirect($req->input());
        $order = $this->order_repo->getByTransactionRef($req->input('objectId'));

        if (!$order) {
            return \response('order not found',400);
        }

        if ($payment->success()) {
            $this->order_repo->paymentProcess($payment);
            //event(new OrderProcess($order->uuid));
        } else if ($payment->error()) {
            $this->order_repo->paymentCancel($payment);
        }
        //Log::channel('kbank')->info("Redirect::order= {$order->id}:{$order->ref_id} ", $payment->input());

        // redirect to thank you page
        $uuid = $order ? $order->uuid : '-';
        $url = config('payment.web.thank_url');
        $url = str_replace('{order}', $uuid, $url);

        return redirect($url, 301);
    }

    /**
     * background callback
     * WebHook Notify API for Card Payment
     */
    public function background(Request $req)
    {
        // payment processing
        $payment = PGwKBank::decode($req->input());

        if ($payment->success()) {
            $order = $this->order_repo->paymentApprove($payment);
           /* if ($order) {
                event(new OrderSuccess($order->uuid, $order->product_sku));
            }*/
        }

        // response
        return $this->response('ok');
    }
}
