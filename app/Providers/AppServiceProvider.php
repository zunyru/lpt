<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use TCG\Voyager\Facades\Voyager;
use App\FormFields\SummernoteFormField;
use App\FormFields\SlugFormField;
use App\FormFields\MyMultiSelect;
use App\FormFields\DateTime;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Voyager::addFormField(SummernoteFormField::class);
        Voyager::addFormField(SlugFormField::class);
        Voyager::addFormField(MyMultiSelect::class);
        Voyager::addFormField(DateTime::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }
}
