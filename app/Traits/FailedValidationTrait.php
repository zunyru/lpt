<?php

namespace App\Traits;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use stdClass;

trait FailedValidationTrait
{
    /**
     * Handle a failed validation attempt.
     *
     * @param  Validator $validator
     * @return void
     *
     */
    protected function failedValidation(Validator $validator)
    {
        $err = (new ValidationException($validator));

        $resObj = new stdClass();
        $resObj->data = [];
        $resObj->success = false;
        $resObj->count = 0;
        $resObj->message = messageBagToMessageString($err->validator->getMessageBag());
        $resObj->status = JsonResponse::HTTP_UNPROCESSABLE_ENTITY;
        $resObj->errors = $err->errors();

        throw new HttpResponseException(
            response()->json($resObj, JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
}
