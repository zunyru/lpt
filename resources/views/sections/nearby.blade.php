<div id="section-buy" class="section page-section footer-stick">
    <div class="container clearfix">
        <div id="section-features" class="heading-block title-center page-section">
            <h2>สถานที่ใกล้เคียง</h2>
            <span style="color:#5a0000;">มูลนิธิศิษย์หลวงพ่อทวดเขาใหญ่</span>
        </div>

        <div class="col_one_third">
            <div class="feature-box fbox-plain">
                <div class="fbox-icon" data-animate="bounceIn">
                    <a href="#"><img src="images/icons/features/tick.png" alt="อิมแพค เมืองทองธานี"></a>
                </div>
                <h3>พิชญ์เขาวงกต</h3>
                <p>&nbsp;</p>
            </div>
        </div>

        <div class="col_one_third">
            <div class="feature-box fbox-plain">
                <div class="fbox-icon" data-animate="bounceIn" data-delay="200">
                    <a href="#"><img src="images/icons/features/tick.png" alt="สถาบันการจัดการปัญญาภิวัฒน์"></a>
                </div>
                <h3 data-delay="200">Bonanza</h3>
                <p>&nbsp;</p>
            </div>
        </div>

        <div class="col_one_third col_last">
            <div class="feature-box fbox-plain">
                <div class="fbox-icon" data-animate="bounceIn" data-delay="400">
                    <a href="#"><img src="images/icons/features/tick.png" alt="มหาวิทยาลัยสุโขทัยธรรมธิราช"></a>
                </div>
                <h3 data-delay="400">Bonanza exotic park</h3>
                <p>&nbsp;</p>
            </div>
        </div>

        <div class="clear"></div>

        <div class="col_one_third">
            <div class="feature-box fbox-plain">
                <div class="fbox-icon" data-animate="bounceIn" data-delay="600">
                    <a href="#"><img src="images/icons/features/tick.png" alt="ศูนย์ราชการเฉลิมพระเกียรติ ๘๐ พรรษา"></a>
                </div>
                <h3 data-delay="600">วัดศิมาลัยทรงธรรม</h3>
                <p>&nbsp;</p>
            </div>
        </div>

        <div class="col_one_third">
            <div class="feature-box fbox-plain">
                <div class="fbox-icon" data-animate="bounceIn" data-delay="800">
                    <a href="#"><img src="images/icons/features/tick.png" alt="กรมการกงสุล กระทรวงต่างประเทศ"></a>
                </div>
                <h3 data-delay="800">ฟาร์มหมอปอ</h3>
                <p>&nbsp;</p>
            </div>
        </div>

        <div class="col_one_third col_last">
            <div class="feature-box fbox-plain">
                <div class="fbox-icon" data-animate="bounceIn" data-delay="1000">
                    <a href="#"><img src="images/icons/features/tick.png" alt="แมคโคร สาขาแจ้งวัฒนะ"></a>
                </div>
                <h3 data-delay="1000">สวนสัปปายะ</h3>
                <p>&nbsp;</p>
            </div>
        </div>

        <div class="clear"></div>

        <div class="col_one_third">
            <div class="feature-box fbox-plain">
                <div class="fbox-icon" data-animate="bounceIn" data-delay="1200">
                    <a href="#"><img src="images/icons/features/tick.png" alt="เซ็นทรัล แจ้งวัฒนะ"></a>
                </div>
                <h3 data-delay="1200">หอศิลป์ประหยัด พงษ์ดำ</h3>
                <p>&nbsp;</p>
            </div>
        </div>

        <div class="col_one_third">
            <div class="feature-box fbox-plain">
                <div class="fbox-icon" data-animate="bounceIn" data-delay="1400">
                    <a href="#"><img src="images/icons/features/tick.png" alt="เซ็นทรัล แจ้งวัฒนะ"></a>
                </div>
                <h3 data-delay="1200">เขาวงกต</h3>
                <p>&nbsp;</p>
            </div>
        </div>

        <div class="col_one_third col_last">
            <div class="feature-box fbox-plain">
                <div class="fbox-icon" data-animate="bounceIn" data-delay="1600">
                    <a href="#"><img src="images/icons/features/tick.png" alt="แมคโคร สาขาแจ้งวัฒนะ"></a>
                </div>
                <h3 data-delay="1000">พุทธอุทยาน อาณาจักรหลวงปูทวด</h3>
                <p>&nbsp;</p>
            </div>
        </div>

        <div class="clear"></div>
    </div>
</div>
