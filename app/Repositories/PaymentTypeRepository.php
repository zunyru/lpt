<?php

namespace App\Repositories;

use App\Models\PaymentType;

class PaymentTypeRepository
{
    public function index()
    {
        return PaymentType::query()
            ->get();
    }

    public function getPaymentTypeById($id)
    {
        return PaymentType::query()
            ->find($id);
    }
}
