<?php


namespace App\Repositories;


use App\Libs\Payments\PaymentEnum;
use App\Models\Transaction;
use App\Libs\Payments\Response as PaymentResponse;

class OrderRepository
{
    public function getByRefId($uuid)
    {
        return Transaction::query()
            ->find($uuid);
    }

    public function paymentProcess(PaymentResponse $payment)
    {
        $order = $this->getByPaymentResp($payment);
        if ($order && $order->status === Transaction::STATUS_CREATE) {
            $order->status = Transaction::STATUS_PROCESS;

            $ref = $payment->input('transaction_ref');
            $ref && $order->transaction_ref = $ref;

            try {
                $order->save();
            } catch (\Exception $e) {
                throw $e;
            }
        }

        return $order;
    }

    public function getByPaymentResp(PaymentResponse $resp)
    {
        // kbank only
        if ($resp->gateway() === PaymentEnum::GW_KBANK) {
            return $this->getByTransactionRef($resp->input('charge_id'));
        }

        $code = $this->getPaymentOrderCode($resp->gateway(), $resp->input());
        return ($code)
            ? $this->getByRefId($code)
            : null;
    }

    public function getByTransactionRef($transaction_ref)
    {
        return Transaction::query()
            ->where('transaction_ref', $transaction_ref)
            ->first();
    }

    public function getPaymentOrderCode($gw, $values)
    {
        $code = null;

        switch ($gw) {
            case PaymentEnum::GW_2C2P:
                $code = $values['order_id'] ?? $values['invoice_no'];
                break;
            case PaymentEnum::GW_KRUNGSRI:
                $code = $values['order_no'];
                break;
            case PaymentEnum::GW_KBANK:
                $code = $values['reference_order'];
                break;
        }

        return $code;
    }

    public function paymentCancel(PaymentResponse $payment)
    {
        $order = $this->getByPaymentResp($payment);

        if ($order && $order->status === Transaction::STATUS_CREATE) {
            $order->status = Transaction::STATUS_CANCEL;

            $ref = $payment->input('transaction_ref');
            $ref && $order->transaction_ref = $ref;

            try {
                $order->save();
            } catch (\Exception $e) {
               throw $e;
            }
        }

        return $order;
    }

    public function paymentApprove(PaymentResponse $payment, $allow_cancel = false)
    {
        $order = $this->getByPaymentResp($payment);

        if ($order && ($order->status !== Transaction::STATUS_CANCEL || $allow_cancel)) {
            $order->status = Transaction::STATUS_COMPLETE;
            //$order->paid_at = $payment->paidAt();

            $ref = $payment->input('transaction_ref');
            //$ref && $order->transaction_ref = $ref;
            $ref && $order->uuid = $ref;

            try {
                $order->save();
            } catch (\Exception $e) {
               throw $e;
            }
        }

        return $order;
    }
}