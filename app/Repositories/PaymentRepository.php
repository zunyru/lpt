<?php

namespace App\Repositories;

use App\Libs\Payments\PaymentEnum;
use App\Libs\Payments\Response as PaymentResponse;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PaymentRepository
{

    protected function storeLog($req, $gw, $verify)
    {
        // prepare data
        if ($req instanceof Request) {
            $data = [
                'server' => $req->server(),
                'header' => $req->header(),
                'method' => $req->method(),
                'input' => $req->input(),
                'query' => $req->query(),
                'content' => $req->getContent(),
            ];
        } else if ($req instanceof PaymentResponse) {
            $data = [
                'input' => $req->input()
            ];
        } else {
            throw new Exception('data not found');
        }

        // get transaction_ref
        $ref = null;
        switch($gw) {
            case PaymentEnum::GW_2C2P_FOREGROUND:
            case PaymentEnum::GW_2C2P_BACKGROUND:
                $ref = $req->input('transaction_ref', '');
                break;
            case PaymentEnum::GW_2C2P_INQUIRY:
                $ref = $req->input('invoice_no', '');
                break;
        }

        // insert log
        $item = [
            'gw' => $gw,
            'ref' => $ref,
            'verify' => $verify,
            'data' => json_encode($data),
            'created_at' => Carbon::now()
        ];

        return DB::table('payment_logs')
            ->insert($item);
    }

    public function log($req, $gw = null, $verify = null)
    {
        try {
            return $this->storeLog($req, $gw, $verify);
        } catch (\Exception $e) {
            return null;
        }
    }

}
