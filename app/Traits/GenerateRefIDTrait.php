<?php

namespace App\Traits;

use Illuminate\Support\Str;

trait GenerateRefIDTrait
{
    protected static function bootUsesRefID()
    {
        static::creating(function ($model) {
            if (empty($model->ref_id)) {
                $model->ref_id = (string) crc32(Str::uuid());
            }
        });
    }

    protected static function bootUsesShortRefID()
    {
        static::creating(function ($model) {
            if (empty($model->ref_id)) {
                $model->ref_id = self::quickRandom(6);
            }
        });
    }

    public static function quickRandom($length = 16,$only_lower = false)
    {
        $pool = 'abcdefghijklmnopqrstuvwxyz';
        $pool .= ($only_lower)
            ? ''
            : '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        return substr(str_shuffle(str_repeat($pool, 5)), 0, $length);
    }
}
