<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Traits\Resizable;
use Illuminate\Database\Eloquent\Builder;


class Banner extends Model
{
    use SoftDeletes, Resizable;

    protected $dates = ['deleted_at'];

    protected $fillable = ['*'];

    /**
     * Log
     */
    protected static $logName = 'Banner';

    protected static $logAttributes = ['*'];

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    public function getDescriptionForEvent(string $eventName): string
    {
        return "This model Banner to {$eventName}";
    }

    public function scopePage(Builder $query, $page)
    {
        return $query->where('page', $page);
    }
}
