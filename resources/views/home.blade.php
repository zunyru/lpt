@extends('layouts.app')

@section('content')
    @include('layouts.slider')

    <section id="content">
        <div class="content-wrap">
            <div class="container clearfix">
                <!-- foundation -->
            @include('sections.foundation')
            <!-- foundation end -->
                <!-- gallery -->
            @include('sections.gallery')
            <!-- gallery end -->
                <!-- contact -->
            @include('sections.contact')
            <!-- contact end -->
            @include('sections.nearby')
            <!-- nearby end -->
            </div>
        </div>

    </section>
@endsection