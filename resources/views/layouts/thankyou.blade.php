@extends('layouts.app')

@section('content')
    <div class="container" style="margin-top:5%;">
        <div class="row">
            <div class="jumbotron" style="box-shadow: 2px 2px 4px #000000;">
                <h2 class="text-center">ขอบคุณสำหรับการบริจาค</h2>
                <h3 class="text-center">Thank you for your donated, it’s processing</h3>

                <p class="text-center">เลขที่ : {{ $no }}</p>
                <p class="text-center">
                    @if($status)
                        {{ "ได้รับเงินบริจาคเรียบร้อยแล้ว" }}
                    @else
                        {{ "ดำเนินการไม่สำเร็จ" }}
                    @endif
                </p>
                <center>
                    <div class="btn-group" style="margin-top:50px;">
                        <a href="{{ url('/') }}" class="btn btn-lg btn-warning">กลับไปสู่หน้าหลัก</a>
                    </div>
                </center>
            </div>
        </div>
    </div>
@endsection