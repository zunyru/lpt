<?php

namespace App\Repositories;

use App\Libs\Payments\PaymentEnum;
use App\Libs\Payments\Response as PaymentResponse;
use App\Models\Transaction;
use Illuminate\Support\Str;

class TransactionRepository
{

    public function getById($uuid)
    {
        return Transaction::query()
            ->find($uuid);
    }

    public function store($param)
    {
        $table = new Transaction();
        $table->uuid = Str::uuid();
        $table->name = $param['name'];
        $table->phone = $param['phone'];
        $table->user_agent = $param['user_agent'];
        $table->payment_type = $param['payment_type'];
        $table->ip = $param['ip'];
        $table->status = Transaction::STATUS_CREATE;
        $table->amount = (double)$param['amount'];

        try {
            $table->save();
        } catch (\Exception $e) {
            throw $e;
        }

        $table->refresh();

        return $table;
    }

    public function getByRefId($ref_id)
    {
        $code = substr($ref_id, -6);
        $seq = intval($code);
        return Transaction::query()
            ->where('sequence_no', $seq)
            ->first();
    }

    public function getByPaymentResp(PaymentResponse $resp)
    {
        // kbank only
        if ($resp->gateway() === PaymentEnum::GW_KBANK) {
            return $this->getByTransactionRef($resp->input('charge_id'));
        }

        $code = $this->getPaymentOrderCode($resp->gateway(), $resp->input());

        return ($code)
            ? $this->getByRefId($code)
            : null;
    }

    public function storeLogPayment($payment)
    {
        $order = $this->getByPaymentResp($payment);
        $order->payment_logs = $payment->input();
        $order->save();
    }

    public function getByTransactionRef($transaction_ref)
    {
        return Transaction::query()
            ->where('transaction_ref', $transaction_ref)
            ->first();
    }

    public function paymentProcess(PaymentResponse $payment)
    {
        $order = $this->getByPaymentResp($payment);
        if ($order && $order->status === Transaction::STATUS_CREATE) {
            $order->status = Transaction::STATUS_PROCESS;

            $ref = $payment->input('transNo');
            $ref && $order->trans_no = $ref;


            $order->save();
        }

        return $order;
    }

    public function paymentApprove(PaymentResponse $payment, $allow_cancel = false)
    {
        $order = $this->getByPaymentResp($payment);

        if ($order && ($order->status !== Transaction::STATUS_CANCEL || $allow_cancel)) {
            $order->status = Transaction::STATUS_COMPLETE;
            //$order->paid_at = $payment->paidAt();

            $ref = $payment->input('TransactionId');
            $ref && $order->trans_no = $ref;

        }

        $order->payment_type  = $payment->input('BankCode') ?? '';

        $order->save();

        return $order;
    }

    public function paymentCancel(PaymentResponse $payment)
    {
        $order = $this->getByPaymentResp($payment);

        if ($order && $order->status === Transaction::STATUS_CREATE) {
            $order->status = Transaction::STATUS_CANCEL;

            $ref = $payment->input('transNo');
            $ref && $order->trans_no = $ref;

            $order->save();
        }

        return $order;
    }

    /**
     * utils
     */

    public function getPaymentOrderCode($gw, $values)
    {
        $code = null;

        switch ($gw) {
            case PaymentEnum::GW_2C2P:
                $code = $values['order_id'] ?? $values['invoice_no'];
                break;
            case PaymentEnum::GW_KRUNGSRI:
                $code = $values['order_no'];
                break;
            case PaymentEnum::GW_CHILLPAY:
                $code = $values['orderNo'];
                break;
            case PaymentEnum::GW_KBANK:
                $code = $values['reference_order'];
                break;
        }

        return $code;
    }

    public function getBankAccountNo($order)
    {
        $id = $order->payment_type_id;
        $accounts = config('payment.accounts');
        return $accounts[$id] ?? '';
    }


}
