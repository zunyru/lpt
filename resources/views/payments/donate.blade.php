@extends('layouts.app')

@section('content')
    <div class="container ">

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="jumbotron"
             style="margin-top: 30px;background: #fff !important;box-shadow: 0 .5rem 1rem rgba(0,0,0,.15)!important;">
            <div class="container ">
                <form action="{{ route('donation') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="name">ชื่อผู้บริจาค</label>
                        <input type="text" class="form-control" id="name" name="name"
                               placeholder="เว้นว่างไว้ หากไม่ประสงค์ออกนาม" value="{{ old('name') }}">
                    </div>
                    <div class="form-group">
                        <label for="name">เบอร์โทรศัพท์</label>
                        <input type="text" class="form-control" id="phone" name="phone"
                               placeholder="เบอร์โทรศัพท์"  value="{{ old('phone') }}">
                    </div>
                    <div class="form-group">
                        <label for="amount">จำนวนเงินที่จะบริจาค</label>
                        <input type="number" step="0.1" class="form-control" id="amount" placeholder="จำนวนเงิน" name="amount" value="{{ old('amount') }}">
                    </div>
                    <div class="form-group text-center">
                        <button type="submit"
                                class="button button-3d button-teal button-large nobottommargin tada animated">บริจาค
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection