<html lang="th">
<head>
    <title>มูลนิธิศิษย์หลวงพ่อทวดเขาใหญ่ หลวงพ่อทวดเขาใหญ่ องค์ใหญ่ที่สุดในโลก @yield('title')</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="author" content="ImpactResidence"/>
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic"
          rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('style.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('css/swiper.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('css/dark.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('css/font-icons.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('css/animate.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('css/magnific-popup.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('css/responsive.css')}}" type="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <!--[if lt IE 9]>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
</head>

<body class="stretched">

<div id="wrapper" class="clearfix">
    @include('layouts.menu')
    @yield('content')
</div>
@include('layouts.footer')

@include('layouts.script')
</body>
</html>