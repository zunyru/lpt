<footer id="footer" class="dark">
    <div id="copyrights">
        <div class="container clearfix">

            <div class="col_half">
                Copyrights &copy; 2020 All Rights Reserved
                <div class="copyright-links"><a href="#">Terms of Use</a></div>
            </div>

            <div class="col_half col_last tright">
                <i class="icon-envelope2"></i> contact.lpt.or.th@gmail.com <span class="middot">&middot;</span> <i class="icon-headphones"></i> <a href="tel:+66988836808" style="color:#fff;">089-883-6808</a>
            </div>

        </div>
    </div>
</footer>
