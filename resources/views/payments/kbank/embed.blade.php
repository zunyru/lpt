<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
</head>

<body>
<form method="POST" action="{{$action_url}}">
    <input type="hidden" name="ref_id" value="{{$ref_id}}">
    <script type="text/javascript"
            src="{{$script_url}}"
            data-mid="{{$merchant_id}}"
            data-apikey="{{$public_apikey}}"
            data-amount="{{$amount}}"
            data-currency="{{$currency}}"
            data-payment-methods="{{$method}}"
    >
    </script>
</form>
</body>

</html>
