<?php

use App\Http\Controllers\Payments\GwChillpayController;
use App\Http\Controllers\Payments\GwKBankController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware(['api'])
    ->group(function () {
        Route::get('/payments/chillpay', [GwChillpayController::class, 'request']);

    });*/

Route::post('/payments/chillpay-redirect', [GwChillpayController::class,'redirect'])
    ->name('redirect');

Route::post('/payments/kbank-charge', [GwKBankController::class, 'charge'])
    ->name('payments-kbank-charge');

Route::middleware(['api'])
    ->prefix('bg')
    ->group(function () {
        Route::any('/payments/chillpay', [GwChillpayController::class, 'background']);
        Route::post('/payments/kbank', [GwKBankController::class, 'background']);
    });