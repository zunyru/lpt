<?php

namespace App\Libs\Payments;

class Response
{
    protected $status;
    protected $values;
    protected $gw;

    public function __construct($status, $values = [], $gw)
    {
        $this->status = $status;
        $this->values = $values;
        $this->gw = $gw;
    }

    public function input($k = null)
    {
        return ($k)
            ? $this->values[$k] ?? null
            : $this->values ?? [];
    }

    public function success()
    {
        return $this->status === PaymentEnum::STATUS_SUCCESS;
    }

    public function pending()
    {
        return $this->status === PaymentEnum::STATUS_PENDING;
    }

    public function cancel()
    {
        return $this->status === PaymentEnum::STATUS_CANCEL;
    }

    public function error()
    {
        return $this->status === PaymentEnum::STATUS_ERROR;
    }

    public function gateway()
    {
        return $this->gw;
    }

    public function toString()
    {
        return $this->status;
    }

    public function paidAt()
    {
        return now();
    }
}
