@extends('layouts.app')

@section('content')

    <section id="content">
        <div class="content-wrap">
            <div class="container clearfix mx-auto">

                <form method="POST" action="/checkout">
                    @csrf
                    <input type="hidden" name="order_id" value="{{ $data->uuid }}">
                    <script type="text/javascript"
                            src="{{ env('KBANK_JAVASCRIPT_URL') }}"
                            data-apikey="{{ env('KBANK_APIKEY') }}"
                            data-amount="{{ $amount ?? '100.00' }}"
                            data-currency="THB"
                            data-payment-methods="card"
                            data-name="มูลนิธิศิษย์หลวงพ่อทวดเขาใหญ่"
                            data-mid="{{ env('KBANK_MID') }}"
                    >
                    </script>
                </form>

                {{--<form method="POST" action="/checkout">
                    <input type="hidden" name="order_id" value="{{ $data->uuid }}">

                </form>
                <script type="text/javascript"
                        src="{{ env('KBANK_JAVASCRIPT_URL') }}"
                        data-apikey="{{ env('KBANK_APIKEY') }}"
                        data-amount="{{ $amount ?? '100.00' }}"
                        data-currency="THB"
                        data-payment-methods="card"
                        data-name="มูลนิธิศิษย์หลวงพ่อทวดเขาใหญ่"
                        data-mid="{{ env('KBANK_MID') }}"
                >
                </script>--}}
            </div>
        </div>

    </section>
@endsection
