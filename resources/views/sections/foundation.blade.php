<div id="section-home" class="heading-block title-center nobottomborder page-section">
    <h2>มูลนิธิศิษย์หลวงพ่อทวดเขาใหญ่</h2>
    <span>ได้จดทะเบียนถูกต้อง ตามประมวลกฎหมายแพ่งและพาณิชย์แล้ว</span>
</div>

<div id="section-foundation" class="center bottommargin">
    <a data-animate="tada" class="button button-3d button-teal button-xlarge nobottommargin" style="color: white;"><i class="icon-suitcase"></i>วัตถุประสงค์ของมูลนิธิ</a>
</div>

<div class="col_full bottommargin">
    <h3>มูลนิธิศิษย์หลวงพ่อทวดเขาใหญ่</h3>
    <p style="font-size: 16px;"><a href="https://goo.gl/maps/eQsMbZ6BpH1sWUtNA" target="_blank">ตั้งอยู่ที่ 118
            อาณาจักรหลวงพ่อทวดเขาใหญ่ องค์ใหญ่ที่สุดในโลก ตำบล หมูสี อำเภอปากช่อง นครราชสีมา 30450</a></p>
    <div class="divider divider-short"><i class="icon-circle"></i></div>
    <h3>มูลนิธิศิษย์หลวงพ่อทวดเขาใหญ่ วัตถุประสงค์ของมูลนิธิ คือ</h3>
    <ul class="iconlist iconlist-large iconlist-color">
        <li><i class="icon-ok-sign"></i> สงเสริมสนับสนุน การศึกษา การกีฬา การดนตรี ศิลปวัฒนธรรมอันดี พุทธศาสนา
            และสาธารณสุข
        </li>
        <li><i class="icon-ok-sign"></i> ส่งเสริมสนับสนุน สงเคราะห์ เด็ก คนชรา คนพิการ ผู้ยากไร้และผู้ด้อยโอกาส</li>
        <li><i class="icon-ok-sign"></i> สงเคราะห์ และช่วยบำบัดรักษา ผู้เจ็บป่วย ผู้ประสบภัย จากโรคภัย อุบัติภัย
            และภัยธรรมชาติทั้งปวง
        </li>
        <li><i class="icon-ok-sign"></i> ดำเนินการในกิจกรรมเพื่อการกุศลทั้งปวง เพื่อสาธารณกุศล</li>
        <li><i class="icon-ok-sign"></i> ดำเนินกิจการทางสื่อสารสาระสนเทศ และเผยแพร่ข้อมูลข่าวสาร
            กิจกรรมตามวัตถุประสงค์เพื่อสาธารณกุศล
        </li>
        <li><i class="icon-ok-sign"></i> รับบริจาค บริจาคเงินและทรัพย์สินเพื่อการกุศลสาธารณะ</li>
        <li><i class="icon-ok-sign"></i> ดำเนินกิจกรรมหรือสนับสนุนกิจกรรมอบรมให้ความรู้ค้นคว้า วิจัย
            มอบเกียรติบัตรแก่ผู้ทำคุณประโยชน์สาธารณกุศล ในนามมูลนิธิ
        </li>
        <li><i class="icon-ok-sign"></i> ดำเนินการหรือร่วมมือกับองค์กรการกุศลเพื่อการกุศล และองค์กรสาธารณประโยชน์
            เพื่อสาธารณประโยชน์
        </li>
        <li><i class="icon-ok-sign"></i> เพื่อส่งเสริมการปกครองในระบอบประชาธิปไตยอันมีพระมหากษัตริย์เป็นประมุข
            ด้วยความเป็นกลาง และไม่ให้การสนับสนุนด้านการเงินหรือทรัพย์สินแก่นักการเมืองหรือพรรคการเมืองใด
        </li>
    </ul>

    <div class="line"></div>

    <h4>สามารถร่วมบริจาคได้ ดังนี้</h4>

    <div class="accordion accordion-bg clearfix">
        <div class="toggle toggle-bg">
            <div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i
                        class="toggle-open icon-remove-circle"></i>
                โอนเข้าบัญชีธนาคา Direct Bank Transfer
            </div>
            <div class="togglec"><img src="images/KBank-logo.jpg"> เลขที่ 064-1-60430-5 สาขาแฟชั่นไอส์แลนด์ รามอินทรา
            </div>
        </div>
        <div class="toggle toggle-bg">
            <div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i
                        class="toggle-open icon-remove-circle"></i>
                ตัดบัตรเครดิต Credit Card By KBank
            </div>
            <div class="togglec">
                {{--kbank--}}
                <form action="{{ route('kbank-view') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="name">ชื่อผู้บริจาค Donor Name</label>
                        <input type="text" class="form-control" id="name" name="name"
                               placeholder="เว้นว่างไว้ หากไม่ประสงค์ออกนาม" value="">
                    </div>
                    <div class="form-group">
                        <label for="name">เบอร์โทรศัพท์ Phone Number</label>
                        <input type="text" class="form-control" id="phone" name="phone" placeholder="เบอร์โทรศัพท์"
                               value="">
                    </div>
                    <div class="form-group">
                        <label for="amount">จำนวนเงินที่จะบริจาค Donation Amount</label>
                        <input required type="number" step="0.1" class="form-control" id="amount" placeholder="จำนวนเงิน"
                               name="amount" value="">
                    </div>
                    <div class="form-group text-center">
                        <button type="submit"
                                class="button button-3d button-teal button-large nobottommargin tada animated">บริจาค donate
                        </button>
                    </div>
                </form>
                {{--end kbank--}}
            </div>
        </div>
        <div class="toggle toggle-bg">
            <div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i
                        class="toggle-open icon-remove-circle"></i>Check Payment By ChillPay
            </div>
            <div class="togglec">
                <div class="alert alert-success">
                    <i class="icon-pencil2"></i><a href="https://lpt.or.th/donate" class="alert-link"
                                                   target="_blank"><strong>คลิกร่วม</strong> บริจาคผ่านระบบ ChillPay</a>.
                </div>
            </div>
        </div>
    </div>
</div>

<div class="clear"></div>

<div class="divider divider-short divider-center"><i class="icon-circle"></i></div>
