<?php

namespace App\Libs\Payments;

interface PaymentGwInterface
{
    public static function request($order_id, $amount, $description = '', $params = []);

    public static function decode($params);

    public static function inquiry($order_id);
}
