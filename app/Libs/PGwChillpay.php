<?php


namespace App\Libs;


use App\Libs\Payments\PaymentEnum;
use App\Libs\Payments\PaymentGwInterface;
use Illuminate\Support\Arr;
use App\Libs\Payments\Response;

class PGwChillpay implements PaymentGwInterface
{

    const STATUS_SUCCESS = 0;
    const STATUS_FAIL = 1;
    const STATUS_CANCEL = 2;
    const STATUS_ERROR = 3;
    const STATUS_PENDING = 9;

    const INQUIRY_STATUS_SETTLED = '00';

    public static function request($ref_id, $amount, $description = '', $params = [])
    {
        $req = new PGwChillpayRequest($ref_id, $amount, $description, $params);
        $values = $req->values();
        // TODO: Implement request() method.
        return Arr::only($values, [
            'MerchantCode',
            'OrderNo',
            'CustomerId',
            'Amount'
        ]);
    }

    public static function decode($params)
    {
        $return = $params['respCode'] ?? self::STATUS_FAIL;

        switch ($return) {
            case self::STATUS_SUCCESS:
                $status = PaymentEnum::STATUS_SUCCESS;
                break;
            case self::STATUS_FAIL:
                $status = PaymentEnum::STATUS_CANCEL;
                break;
            case self::STATUS_PENDING:
                $status = PaymentEnum::STATUS_PENDING;
                break;
            default:
                $status = PaymentEnum::STATUS_ERROR;
                break;
        }

        return new Response($status, $params, PaymentEnum::GW_CHILLPAY);
    }

    public static function inquiry($order_id)
    {
        // TODO: Implement inquiry() method.
    }

    public function verify($params)
    {

        $ver = new PGwChillpayVerify($params);
        $result = $ver->get();
        $status = ($result)
            ? PaymentEnum::STATUS_SUCCESS
            : PaymentEnum::STATUS_ERROR;
    }

    public static function decode_bg($params)
    {
        $return = $params['PaymentStatus'] ?? self::STATUS_FAIL;

        switch ($return) {
            case self::STATUS_SUCCESS:
                $status_payment = PaymentEnum::STATUS_SUCCESS;
                break;
            case self::STATUS_CANCEL:
                $status_payment = PaymentEnum::STATUS_CANCEL;
                break;
            case self::STATUS_PENDING:
                $status_payment = PaymentEnum::STATUS_PENDING;
                break;
            default:
                $status_payment = PaymentEnum::STATUS_ERROR;
                break;
        }

        $params['orderNo'] = $params['OrderNo'];

        if($status_payment != PaymentEnum::STATUS_SUCCESS)
        {
            return new Response($status_payment, $params, PaymentEnum::GW_CHILLPAY);
        }

        $ver = new PGwChillpayVerify($params);
        $result = $ver->get();
        $status = ($result)
            ? PaymentEnum::STATUS_SUCCESS
            : PaymentEnum::STATUS_ERROR;



        return new Response($status, $params, PaymentEnum::GW_CHILLPAY);

    }
}
