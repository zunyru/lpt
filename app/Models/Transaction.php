<?php

namespace App\Models;

use App\Traits\GenerateRefIDTrait;
use App\Traits\GenerateUUIDTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;

class Transaction extends BaseModel
{
    use SoftDeletes;
    use GenerateUUIDTrait;
    use GenerateRefIDTrait;

    const STATUS_CREATE = 'create';
    const STATUS_PROCESS = 'process';
    const STATUS_COMPLETE = 'complete';
    const STATUS_CANCEL = 'cancel';

    const STATUS_DEPOSIT_OK = 'ok';
    const STATUS_SALE_ORDER_OK = 'ok';

    protected $primaryKey = 'uuid';

    protected $keyType = 'string';

    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        self::bootUsesUuid();
    }

    protected $appends = [
        'ref_id'
    ];


    protected $dates = [
        'created_at',
        'updated_at'
    ];

    public function getRefIdAttribute()
    {
        $date = Carbon::rawParse($this->created_at)->format('ym');
        $sequence_no = str_pad($this->sequence_no, 6, '0', STR_PAD_LEFT);
        $prefix = config('transaction.prefix');

        return "{$prefix}{$date}{$sequence_no}";
    }
}
