<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->uuid('uuid')->primary();
            $table->bigInteger('sequence_no')->unique();
            $table->string('name', 255)->nullable();
            $table->string('phone', 50)->nullable();
            $table->decimal('amount', 12, 2)->default(0);
            $table->string('ip')->nullable();
            $table->mediumText('user_agent')->nullable();
            $table->string('status', 150);
            $table->string('payment_type', 50)->nullable();
            $table->bigInteger('trans_no')->nullable();
            $table->longText('payment_logs')->nullable();
            $table->longText('transaction_ref')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->index(['uuid','payment_type','deleted_at'],'orders_payment_index');
        });


        DB::statement('ALTER TABLE transactions MODIFY sequence_no INTEGER NOT NULL AUTO_INCREMENT');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
